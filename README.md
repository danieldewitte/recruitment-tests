# Coding Assessment

Please organize, design, test, document and deploy your code as if it were going into production, then send us a link to the hosted repository (e.g. Gitlab, Github...) as well as a link to the hosted version.

## Functional spec

Prototype **one** of the following projects:

1. Authentication Service
2. Email Service
3. Liquidity Provisioning (does not interact with a backend but with a blockchain)

The UX/UI is totally up to you. If you like, get creative and add additional
features a user might find useful!

### Authentication Service

Create a NodeJS backend service that allows a user to register and validate an account and also login and obtain a jwt token.

Create a Simple screen that allows for registration, verification, and login

### Email Service

Create a service that take in the information required to send an email. It
should provide an abstraction of the email service provider, so do not re-expose their datamodel.

Create a simple screen to execute the service.

Email Provider:

- Preferably use Sendgrid, but an alternative is possible too.

Register your own test accounts for the email service.

### Liquidity Provisioning

Create a screen that interacts with a decentralized exchange that uses liquidity pools and make it possible to provide liquidity to the smart contract.

Import the ABI of the DEX

Use the testnet of an EVM compatible blockchain.
Make sure the DEX you build on has smart contracts deployed on that testnet.

## Technical spec

The application will be have both a back-end and a web front-end.

### Back-end

_Only applies to project 1 and 2, not to 3_

As we have a defined toolstack we would like to see technologies we use, however if you do not possess them we do not want to exclude you directly as wel believe mentality, energy and the willingness to learn contribute to good software as much as the choice of tools.

- Use Typescript (this is the only hard requirement)
- If you have experience with Graphql use that, otherwise use REST.
- If you have experience with NestJS use that, otherwise use a framework of choice.
- If you require a relational database preferably use Postgress or something else opensource.
- If you require a key-value store preferably use Redis but any other will do.

### Front-end

The front-end should ideally be a single page app with a single `index.html`
linking to external JS/CSS/etc. You may take this opportunity to demonstrate
your CSS3 or HTML5 knowledge.'

- We prefer to see React being used as frontend framework, but Angular or Vue will not disqualify you.

## Hosting

When you’re done, please host it somewhere and share the link.
